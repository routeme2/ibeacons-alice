//
//  ViewController.swift
//  iBeacons
//
//  Created by Alice Aimee Alvarado.
//  Copyright © 2017 alaialva. All rights reserved.
//

import UIKit
import CoreLocation // Library to interact with location
import AVFoundation // For Audio and Video

class ViewController: UIViewController, CLLocationManagerDelegate { // tells class what delegates it is able to work with (cllocationManagerDelegate)
    
    // iBeacons' information --------------------------------------------------
    
    // iBeacon: pWEE (A)
    // UUID: F7826DA6-4FA2-4E98-8024-BC5B71E0893E
    // Major: 9073
    // Minor: 1
    
    // iBeacon: XHzQ (B)
    // UUID: F7826DA6-4FA2-4E98-8024-BC5B71E0893E
    // Major: 9073
    // Minor: 2
    
    // iBeacon: 33tM (C)
    // UUID: F7826DA6-4FA2-4E98-8024-BC5B71E0893E
    // Major: 9073
    // Minor: 3
    
    // Bus beacons:
    // z1dy = 59182
    // mnZr = 18457
    // pYEM = 59083
    // d2Rq = 48048
    
    
    // Audio Players ---------------------------------------------------------
    
    //var goForwardPlayer: AVAudioPlayer = AVAudioPlayer()
    //var wrongBusStopPlayer: AVAudioPlayer = AVAudioPlayer()
    //var waitPlayer: AVAudioPlayer = AVAudioPlayer()
    //var busIsHerePlayer: AVAudioPlayer = AVAudioPlayer()
    //var departBusStopPlayer: AVAudioPlayer = AVAudioPlayer()
    

    // Variables and Labels --------------------------------------------------
    
    // Select region to monitor our own iBeacons from kontakt.io
    // by using our UUID and identifing them as kontakt beacons
    // For monitoring a specific iBeacon include that iBeacon's major and minor
    let regionToMonitor = CLBeaconRegion(proximityUUID: UUID(uuidString:"F7826DA6-4FA2-4E98-8024-BC5B71E0893E")!, identifier: "kontakt beacons")
    
    // Declare a location Manager that coordinates all locations
    let locationManager = CLLocationManager()
    
    // Detection Status
    @IBOutlet weak var detectionLabel: UILabel!
    
    // Was data stored in the file?
    @IBOutlet weak var dataStoredLabel: UILabel!

    // Immediate RSSI value read
    @IBOutlet weak var rssiLabel: UILabel!
    
    // Latitude and Longitude
    @IBOutlet weak var latitudeLongitudeLabel: UILabel!
    
    // Input of the degree data is taken from
    @IBOutlet weak var degreeInput: UITextField!
    
    // Input of the radius data is taken from
    @IBOutlet weak var distanceInput: UITextField!
    
    // Input of offset (at which angle you are facing from the desired spot)
    @IBOutlet weak var offsetInput: UITextField!
    
    // Bus label indicates if bus is near
    @IBOutlet weak var bus1: UILabel!
    
    // File's name that will contain the data collected
    var filename:String = ""
    
    // Iterate through the list of iBeacons and display their properties on the label
    // String is also used to gather indiviual iBeacon info depending on the region to monitor above
    var beaconInfo:String = ""
    var beaconInfo2: String = ""
    
    // Variables to store the beacon rssi values and adverages
    var bA = 0 // Beacon A
    var bB = 0 // Beacon B
    var bC = 0 // Beacon C
    
    var bA1 = 0 // Beacon A
    var bB1 = 0 // Beacon B
    var bC1 = 0 // Beacon C
    
    var LRSSI = 0 // the addition of the current RSSI values
    var threshold = -240 // threshold for determining if user is at bus stop

    var i = 0 // counter of how many rssi reading were taken by iBeacon A
    var j = 0 // counter of how many rssi reading were taken by iBeacon B
    var k = 0 // counter of how many rssi reading were taken by iBeacon C
    
    var tA = 0 // the average RSSI values of iBeacon A
    var tB = 0 // the average RSSI values of iBeacon B
    var tC = 0 // the average RSSI values of iBeacon C
    
    // JSON parameters
    var busStopName:String = "unknown"
    var transitName:String = "none"
    var nearest:String = "true"
    var mode:String = "unknown"
    var tripInstanceID:String = "7564153e33aadd0ba033a9ee7b624a89"
    var travelerID:String = "50965d1fa6e7625057c1615418819dc1b9f89139924646397b43969c9ac27a17"

    // Audio Players ---------------------------------------------------------
    
    var goForwardPlayer: AVAudioPlayer = AVAudioPlayer()
    var wrongBusStopPlayer: AVAudioPlayer = AVAudioPlayer()
    var waitPlayer: AVAudioPlayer = AVAudioPlayer()
    var busIsHerePlayer: AVAudioPlayer = AVAudioPlayer()
    var departBusStopPlayer: AVAudioPlayer = AVAudioPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Initializing audio files to their players
        do {
            let goForwardPath = Bundle.main.path(forResource: "goforward", ofType: "aiff")
            let wrongBusStopPath = Bundle.main.path(forResource: "wrongBustop", ofType: "aiff")
            let waitPath = Bundle.main.path(forResource: "wait", ofType: "aiff")
            let busIsHerePath = Bundle.main.path(forResource: "busIsHere", ofType: "aiff")
            let departBusStopPath = Bundle.main.path(forResource: "departBus", ofType: "aiff")
            
            try goForwardPlayer = AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: goForwardPath!) as URL)
            try wrongBusStopPlayer = AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: wrongBusStopPath!) as URL)
            try waitPlayer = AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: waitPath!) as URL)
            try busIsHerePlayer = AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: busIsHerePath!) as URL)
            try departBusStopPlayer = AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: departBusStopPath!) as URL)
            
        }catch{
            // error
            print("error: Audio files didn't match their players")
        }

        
        // Creating a DONE button on the keyboards
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done , target: self, action: #selector(self.doneClicked))
        toolBar.setItems([doneButton], animated: false)
        degreeInput.inputAccessoryView = toolBar
        distanceInput.inputAccessoryView = toolBar
        offsetInput.inputAccessoryView = toolBar
        
        // Do any additional setup after loading the view, typically from a nib.
        // Let this class receive location updates
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        
        // Request authorization to use location services. Manually done in iOS settings.
        if (CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedWhenInUse){
            locationManager.requestWhenInUseAuthorization()
        }
        
        // Day and time the file is created
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "MMddyyyy-hhmmss"
        let dateStringFormat = dayTimePeriodFormatter.string(from: Date())
        
        // Name of file created to store the data collected
        filename = "FingerPrintingTable" + dateStringFormat + ".txt"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func doneClicked(){
        view.endEditing(true)
    }
    
    // Receive iBeacon updates on their current informatiom
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        
        // Grabs iBeacons' information wanted
        for beacon in beacons {
            
            // Grabing the RSSI value and displaying it
            rssiLabel.text = "RSSI: \(beacon.rssi)"
            
            enum State {
                case Start, GuideToBus, North, South, SeeBus, OnBus, Destination
            }
            
            var state = State.Start
            
            switch state{
            case .Start:
                if (LRSSI < threshold){
                    busStopName = "north_science_hill_busstop"
                    // say North Science Hill Bus Stop
                    state = .North
                }
                if (beacon.minor == 10655){
                    busStopName = "south_science_hill_busstop"
                    // say North Science Hill Bus Stop
                    state = .South
                }
                if (beacon.minor != 1 && beacon.minor != 2 && beacon.minor != 3){
                    state = .GuideToBus
                    // send raw gps
                }
                
            // case .GuideToBus:
                
            case .North:
                if (beacon.minor == 59182){   //z1dy
                    // Say you see bus
                    state = .SeeBus
                }
                if (beacon.minor == 18457){   //mnZr
                    // Say you see bus
                    state = .SeeBus
                }
                if (beacon.minor == 59083){   //pYEM
                    // Say you see bus
                    state = .SeeBus
                }
                if (beacon.minor == 48048){   //d2Rq
                    // Say you see bus
                    state = .SeeBus
                }
            case .South:
                if (beacon.minor == 59182){   //z1dy
                    // Say you see bus
                    state = .SeeBus
                }
                if (beacon.minor == 18457){   //mnZr
                    // Say you see bus
                    state = .SeeBus
                }
                if (beacon.minor == 59083){   //pYEM
                    // Say you see bus
                    state = .SeeBus
                }
                if (beacon.minor == 48048){   //d2Rq
                    // Say you see bus
                    state = .SeeBus
                }
                
            case .SeeBus:
                if (beacon.minor != 1 && beacon.minor != 2 && beacon.minor != 3){
                    if (beacon.minor == 59182){   //z1dy
                        transitName = "921"
                        mode = "bus"
                        // Your on route
                        state = .OnBus
                    }
                    if (beacon.minor == 18457){   //mnZr
                        transitName = "928"
                        mode = "bus"
                        // Your on route
                        state = .OnBus
                    }
                    if (beacon.minor == 59083){   //pYEM
                        transitName = "pYEM"
                        mode = "bus"
                        // Your on route
                        state = .OnBus
                    }
                    if (beacon.minor == 48048){   //d2Rq
                        transitName = "d2Rq"
                        mode = "bus"
                        // Your on route
                        state = .OnBus
                    }
                }
            case .OnBus:
                if (beacon.minor != 1 && beacon.minor != 2 && beacon.minor != 3 && beacon.minor != 59182 && beacon.minor != 18457 && beacon.minor != 59083 && beacon.minor != 48048){
                    state = .Destination
                }
            //case .Destination:
                // say you have arrived
                
            }
   
            
            // North Bus Stop beacons
            if (beacon.minor == 1) {
                bA += beacon.rssi // Add up all rssi values
                i += 1 // increament number of rssi reads
                if (beacon.rssi >= -74){
                    busStopName = "north_science_hill_busstop"
                }
            }
            if (beacon.minor == 2){
                bB1 = beacon.rssi // Updates the current value of the beacon's rssi value
                bB += beacon.rssi // Add up all rssi values
                j += 1 // increament number of rssi reads
                if (beacon.rssi >= -74){
                    busStopName = "north_science_hill_busstop"
                }
            }
            if (beacon.minor == 3){
                bC1 = beacon.rssi // Updates the current value of the beacon's rssi value
                bC += beacon.rssi // Add up all rssi values
                k += 1 // increament number of rssi reads
                if (beacon.rssi >= -74){
                    busStopName = "north_science_hill_busstop"
                }
            }
            
            // South Bus Stop beacon
            if (beacon.minor == 10655){
                if (beacon.rssi >= -74){
                    busStopName = "south_science_hill_busstop"
                }
            }
            
            // Bus readings
            // determining weather walikng or on bus 
            // as well as with bus we are on
            
            // on the bus
            if (beacon.minor != 1 && beacon.minor != 2 && beacon.minor != 3){
                if (beacon.minor == 59182){   //z1dy
                    bus1.text = "I see z1dy"
                    transitName = "921"
                    mode = "bus"
                }
                if (beacon.minor == 18457){   //mnZr
                    bus1.text = "I see mnZr"
                    transitName = "928"
                    mode = "bus"
                }
                if (beacon.minor == 59083){   //pYEM
                    bus1.text = "I see pYEM"
                    transitName = "pYEM"
                    mode = "bus"
                }
                if (beacon.minor == 48048){   //d2Rq
                    bus1.text = "I see d2Rq"
                    transitName = "d2Rq"
                    mode = "bus"
                }
            }
            
            // walking
            if (beacon.minor != 59182 && beacon.minor != 18457 && beacon.minor != 59083 && beacon.minor != 48048){
                mode = "waking"
                transitName = "none"
            }
        }
    } // end of location manager
    
    @objc(locationManager:didUpdateLocations:) func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        latitudeLongitudeLabel.text = ("\((locationManager.location?.coordinate.latitude)!),\((locationManager.location?.coordinate.longitude)!)\n")
        
        // prepare json data
            let json: [String: Any] = ["traveler_id":travelerID,"trip_instance_id":tripInstanceID,"latitude":(locationManager.location?.coordinate.latitude)!,"longitude": (locationManager.location?.coordinate.longitude)!,"accuracy_dist":"5","mode":mode, "nearest":nearest,"stop_name":busStopName,"transit_name":transitName]
            
            let jsonData = try? JSONSerialization.data(withJSONObject: json)
            
            //create post request
            let url = URL(string: "https://routeme2app.mybluemix.net/api/check_location")!
            // let url = URL(string: "https://requestb.in/qrf0d8qr")!
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            let originalText:String = "testing@aol.com:testing"
            let encoded = originalText.toBase64()
            //let encodedText = originalText.data(using: .utf8)!
            request.setValue("Basic \(encoded)", forHTTPHeaderField: "authorization")
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            // insert json data to the request
            request.httpBody = jsonData
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data, error == nil else {
                    print(error?.localizedDescription ?? "No data")
                    return
                }
                let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
                if let responseJSON = responseJSON as? [String: Any] {
                    print(responseJSON)
                }
            }
            task.resume()
    }
    
    @IBAction func storeData(_ sender: Any) {
        
        //Locate the document directory and save the data there
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let dirFilePath = dir.appendingPathComponent(filename)
            
            // Write and save the file
            do {
                try beaconInfo.write(to: dirFilePath, atomically: false, encoding: String.Encoding.utf8)
                
                // Reports back to the tester immediate
                dataStoredLabel.text = "Data Stored!"
            } catch _ {
                // Reports back to the tester immediate
                dataStoredLabel.text = "Data Not Stored!"
                
                print("Could not save the file.")
            }
            
            // Read file
            do {
                
                beaconInfo2 = try String(contentsOf: dirFilePath, encoding: String.Encoding.utf8)
            }
            catch {
                print("Could not read the file.")
            }
           // print(beaconInfo2)
        }
    }
    
    @IBAction func startDetecting(_ sender: AnyObject) {
        
        // Begin monitoring
        locationManager.startRangingBeacons(in: regionToMonitor)
        
        // reset beacon rssi values
        bA = 0
        bB = 0
        bC = 0
        
        // counter reset for new distance
        i = 0
        j = 0
        k = 0
        
        // Displaying the detection status
        detectionLabel.text = "Detecting"
    }
    
    @IBAction func stopDetecting(_ sender: AnyObject) {
        
        // Stop monitoring
        locationManager.stopRangingBeacons(in: regionToMonitor)
        
        // Displaying the detection status
        detectionLabel.text = "Not Detecting"
        
        // Average Calculations
        tA = bA/i
        tB = bA/j
        tC = bC/k
        
        // Organize information into an array and add to finger printing table
        beaconInfo2 = "\(tA),\(tB),\(tC)," + degreeInput.text!  + "," + distanceInput.text! + "," + offsetInput.text! + "\n"
        beaconInfo.append(beaconInfo2)
        print(beaconInfo2)
        
    }

}

extension String {
    
    /// Encode a String to Base64
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
    
    /// Decode a String from Base64. Returns nil if unsuccessful.
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else { return nil }
        return String(data: data, encoding: .utf8)
    }
    
}


